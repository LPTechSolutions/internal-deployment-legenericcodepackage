This is a sample, generic code package for Live Engage.  It contains:
1. LEdeployment-example.html -- sample HTML file, with examples of how to send variables (Engagement Attributes) to Live Person, the DIV ID required for dynamic button
2. le2-mtagconfig.js -- JavaScript file which communicates with Live Person
3. LPAttributes -- Engagement Attributes JSON samples



