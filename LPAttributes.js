var LPCustomerInfo = {
	"type": "ctmrinfo",  //MANDATORY customer info
		"info": {
		"cstatus": custStatus,  //CUSTOMER LIFECYCLE STATUS. UserState(UserState)
		"ctype": custType,
		"customerId": custID,
		"balance": custBalance,
		"lastPaymentDate": {
		   "day": custLastPaymentDay,
		   "month": custLastPaymentMonth,
		   "year": custLastPaymentYear
		},
		"registrationDate": {
		   "day": custRegistrationDay,
		   "month": custRegistrationMonth,
		   "year": custRegistrationYear
		}
	}
};

var LPPersonalInfo = {
	"type": "personal",  //MANDATORY
	"personal": {
		"firstname": personalFirstName,
		"lastname": personalLastName,
		"age": {
		   "age": personalAge,
		   "day": personalBirthDay,
		   "month": personalBirthMonth,
		   "year": personalBirthYear
		   },
		"contacts": {
		   "email": personalEmail,
		   "phone": personalPhone
		},
		"gender": personalGender,
		"company": personalCompany 			
    }
};

var LPMarketingSource = {    
	"type": "mrktInfo",  //MANDATORY
	"info": {
		"channel": marketingChannel,    // 0-Direct, 1-Search, 2-Social, 3-Email, 4-Referral, 5-Paid Search, 6-Display
		"affiliate": marketingAffiliate,
		"campaignId": marketingCampaignID
    }
}; 

var LPLead = {    
	"type": "lead",  //MANDATORY
	"lead": {
		"topic": leadTopic,    // 0-Direct, 1-Search, 2-Social, 3-Email, 4-Referral, 5-Paid Search, 6-Display
		"value": leadValue,
		"leadId": leadLeadID
    }
}; 

var LPServiceActivity = {
	"type": "service",  //MANDATORY
	"service": {
		"topic": serviceTopic,
		"status": serviceStatus,
		"category": serviceCategory,
		"serviceId": serviceServiceID 			
    }
};

var LPError = {
	"type": "error",  //MANDATORY
	"error": {
		"message": errorMessage,
		"code": errorCode
    }
};

var LPCart =
	   {    
			"type": "cart",  //MANDATORY
			"total": cartTotal,
			"numItems": cartNumItems,
			"products": [
			
			{
			    "product": {   // Repeat for as many products are in cart, changing the JS variable for each, e.g. cartProduct2Name
					"name": cartProduct1Name,  
					"category": cartProduct1Category,
					"sku": cartProduct1Sku,
					"price": cartProduct1Price
			    },
				"quantity": cartProduct1Quantity,
			},
            {			
			    "product": {   // Repeat for as many products are in cart, changing the JS variable for each, e.g. cartProduct2Name
					"name": cartProduct2Name,  
					"category": cartProduct2Category,
					"sku": cartProduct2Sku,
					"price": cartProduct2Price
			    },				
				
				"quantity": cartProduct2Quantity
		   }
		   ]
	   } ;
                 
var LPOrder = {    
	"type": "purchase",  //MANDATORY
	"total": purchaseTotal,
	"orderId": purchaseOrderID,
	"cart": {
		"products": {
			"product": {   // Repeat for as many products are in cart
				"name": purchaseProduct1Name,  
				"category": purchaseProduct1Category,
				"sku": purchaseProduct1Sku,
				"price": purchaseProduct1Price
			},
			"quantity": purchaseProduct1Quantity
	    }
    } 
}; 	   

var LPProductView = 
	   {    
			"type": "prodView",  //MANDATORY
			"products": [
			{
				"product": {   // Repeat for as many products viewed, same as in LPCart
					"name": prodviewProduct1Name,  
					"category": prodviewProduct1Category,
					"sku": prodviewProduct1Sku,
					"price": prodviewProduct1Price
				}
		   },
			{
				"product": {   
					"name": prodviewProduct2Name,  
					"category": prodviewProduct2Category,
					"sku": prodviewProduct2Sku,
					"price": prodviewProduct2Price
				}
		   }
        ]		   
	   };
			 